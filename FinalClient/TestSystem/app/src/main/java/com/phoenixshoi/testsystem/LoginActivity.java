package com.phoenixshoi.testsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.Socket;
import java.net.UnknownHostException;


public class LoginActivity extends ActionBarActivity {

    EditText loginEditText;
    EditText passwordEditText;

    Button loginButton;

    ClientThread clientThread=null;

    SharedPreferences save;

    Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        intent = new Intent(LoginActivity.this,RunTest.class);

        loginEditText = (EditText) findViewById(R.id.loginEditText);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);

        loginButton = (Button) findViewById(R.id.loginButton);

        save = getPreferences(MODE_PRIVATE);

        if(!save.getString("login","").equals("")){
            intent.putExtra("address",getIntent().getStringExtra("address"));
            intent.putExtra("login",save.getString("login", ""));
            intent.putExtra("password",save.getString("password", ""));

            startActivity(intent);

            finish();
        }
    }

    @Override
    public  void onDestroy(){
        if(clientThread!=null){
            clientThread.disconnect();
        }
        super.onDestroy();
    }

    public void viewMessage(String message){
        Toast.makeText(LoginActivity.this, message, Toast.LENGTH_LONG).show();
    }

    public void onLoginClick(View view) {
        clientThread = new ClientThread(getIntent().getStringExtra("address"),2326,"01_"+loginEditText.getText().toString()+"_"+passwordEditText.getText().toString());
        clientThread.start();
    }

    private class ClientThread extends Thread{
        String dstAddress;
        int dstPort;
        String dstResponse;

        boolean flag=false;

        public ClientThread(String address,int port, String textResponse){
            dstAddress=address;
            dstPort=port;
            dstResponse=textResponse;
        }

        @Override
        public  void run(){
            Socket socket=null;
            DataOutputStream dataOutputStream=null;
            DataInputStream dataInputStream=null;

            try{
                socket = new Socket(dstAddress,dstPort);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataInputStream = new DataInputStream(socket.getInputStream());

                byte[] bytes = dstResponse.getBytes("ASCII");

                bytes[0]=(byte)(bytes.length & 0x0ff);

                dataOutputStream.write(bytes);
                dataOutputStream.flush();

                while (!flag){
                    int length = dataInputStream.readInt();
                    if(length>0){
                        byte[] message = new byte[length];
                        dataInputStream.readFully(message,0,message.length);

                        String answer="";

                        for(int i=0;i<length;i=i+2){
                            answer+=(char)message[i+1];
                        }

                        if(answer.equals("-1")){
                            LoginActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewMessage(getString(R.string.login_accepted));

                                    SharedPreferences.Editor ed = save.edit();
                                    ed.putString("login",loginEditText.getText().toString());
                                    ed.putString("password",passwordEditText.getText().toString());
                                    ed.commit();

                                    intent.putExtra("login", save.getString("login", ""));
                                    intent.putExtra("password", save.getString("password", ""));
                                    intent.putExtra("address",dstAddress);
                                    startActivity(intent);

                                    finish();

                                    flag=true;
                                }
                            });
                        } else {
                            LoginActivity.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewMessage(getString(R.string.login_unaccepted));
                                }
                            });
                        }
                    } else {
                        flag = false;
                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
                e.printStackTrace();
            } finally {
                if(socket!=null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(dataOutputStream!=null){
                    try{
                        dataOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(dataInputStream!=null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        public void disconnect(){
            flag=true;
        }
    }
}
