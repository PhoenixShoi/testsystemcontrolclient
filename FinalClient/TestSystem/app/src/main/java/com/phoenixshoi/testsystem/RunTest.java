package com.phoenixshoi.testsystem;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class RunTest extends Activity {

    ClientThread clientThread=null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_run_test);

        clientThread = new ClientThread(getIntent().getStringExtra("address"),2326,"01_"+getIntent().getStringExtra("login")+"_"+getIntent().getStringExtra("password"));
        clientThread.start();
    }

    public void onClick(View v) {
        String answer="02_"+getIntent().getStringExtra("login")+"_"+getIntent().getStringExtra("password")+"_";
        switch (v.getId()) {
            case R.id.buttonOne:
                answer+="1";
                break;
            case R.id.buttonTwo:
                answer+="2";
                break;
            case R.id.buttonThree:
                answer+="3";
                break;
            default:
                break;
        }
        clientThread = new ClientThread(getIntent().getStringExtra("address"),2326,answer);
        clientThread.start();
    }

    private class ClientThread extends Thread{
        String dstAddress;
        int dstPort;
        String dstResponse;

        public ClientThread(String address,int port, String textResponse){
            dstAddress=address;
            dstPort=port;
            dstResponse=textResponse;
        }

        @Override
        public  void run(){
            Socket socket=null;
            DataOutputStream dataOutputStream=null;

            try{
                socket = new Socket(dstAddress,dstPort);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());

                byte[] bytes = dstResponse.getBytes("ASCII");

                bytes[0]=(byte)(bytes.length & 0x0ff);

                dataOutputStream.write(bytes);
                dataOutputStream.flush();

            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                RunTest.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
                e.printStackTrace();
            } finally {
                if(socket!=null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(dataOutputStream!=null){
                    try{
                        dataOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
