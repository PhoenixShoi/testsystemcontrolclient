package com.phoenixshoi.testsystem;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


public class ConnectServer extends ActionBarActivity {

    EditText ipAddress;
    Button loginButton;

    ClientThread clientThread=null;

    SharedPreferences save;


    SharedPreferences.Editor ed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect_server);
        setTitle("Вход на сервер");

        ipAddress = (EditText) findViewById(R.id.ipAddressEditText);
        loginButton = (Button) findViewById(R.id.loginButton);

        save = getPreferences(MODE_PRIVATE);
        ed = save.edit();

        if(!save.getString("address","").equals("")){
            clientThread = new ClientThread(save.getString("address","").toString(),2326,"00");
            clientThread.start();
        }

    }

    @Override
    public  void onDestroy(){
        if(clientThread!=null){
            clientThread.disconnect();
        }
        super.onDestroy();
    }

    public void viewMessage(String message){
        Toast.makeText(ConnectServer.this,message, Toast.LENGTH_LONG).show();
    }

    public void loginButtonClick(View view) {
        clientThread = new ClientThread(ipAddress.getText().toString(),2326,"00");
        clientThread.start();
    }

    private class ClientThread extends Thread{
        String dstAddress;
        int dstPort;
        String dstResponse;

        boolean flag=false;

        public ClientThread(String address,int port, String textResponse){
            dstAddress=address;
            dstPort=port;
            dstResponse=textResponse;
        }

        @Override
        public  void run(){
            Socket socket=null;
            DataOutputStream dataOutputStream=null;
            DataInputStream dataInputStream=null;

            try{
                socket = new Socket(dstAddress,dstPort);
                dataOutputStream = new DataOutputStream(socket.getOutputStream());
                dataInputStream = new DataInputStream(socket.getInputStream());

                byte[] bytes = dstResponse.getBytes("ASCII");

                bytes[0]=(byte)(bytes.length & 0x0ff);

                dataOutputStream.write(bytes);
                dataOutputStream.flush();

                while (!flag){
                    int length = dataInputStream.readInt();
                    if(length>0){
                        byte[] message = new byte[length];
                        dataInputStream.readFully(message,0,message.length);

                        String answer="";

                        for(int i=0;i<length;i=i+2){
                            answer+=(char)message[i+1];
                        }

                        if(answer.equals("0")){
                            ConnectServer.this.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    viewMessage("Подключено");

                                    Intent intent = new Intent(ConnectServer.this,LoginActivity.class);
                                    intent.putExtra("address",dstAddress);

                                    ed.putString("address",dstAddress);
                                    ed.commit();

                                    startActivity(intent);

                                    finish();
                                    flag=true;
                                }
                            });
                        }
                    } else {
                        flag = false;
                    }
                }
            } catch (UnknownHostException e) {
                e.printStackTrace();
            } catch (IOException e) {
                ConnectServer.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        viewMessage("Сервер не найден попробуйте снова.");
                        ed.putString("address","");
                        ed.commit();
                    }
                });
                e.printStackTrace();
            } finally {
                if(socket!=null){
                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(dataOutputStream!=null){
                    try{
                        dataOutputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if(dataInputStream!=null) {
                    try {
                        dataInputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        public void disconnect(){
            flag=true;
        }
    }
}
